import os
import re
import random
import numpy as np
import networkx as nx
from time import time
from tqdm import tqdm
from multiprocessing.pool import Pool

from functools import partial

import node2vec.src.node2vec as n2v
from src.util import EasyDict
import src.main


# 'atoi' and 'natural_keys' taken from:
# https://stackoverflow.com/questions/5967500/how-to-correctly-sort-a-string-with-a-number-inside


def atoi(text):
    """ Return the digit contained in the text

    :param text:
    :return int:
    """
    return int(text) if text.isdigit() else text


def natural_keys(text):
    """ Returns a list of int read in the text, which must be composed of ints split by spaces

    :param text:
    :return list(int):
    """
    return [atoi(c) for c in re.split('(\d+)', text)]


def random_walk(graph, node, walk_length):
    """ Create a random walk of length walk_length from node

    :param graph:
    :param node:
    :param walk_length:
    :return walk: list of nodes
    """
    walk = [node]
    for i in range(walk_length):
        neighbors = graph.neighbors(walk[i])
        walk.append(random.choice(list(neighbors)))
    return walk


def generate_walks(graph, num_walks, walk_length):
    """ Samples num_walks walks of length walk_length+1 from each node of graph

    :param graph:
    :param num_walks:
    :param walk_length:
    :return walks:
    """
    graph_nodes = graph.nodes()
    n_nodes = len(graph_nodes)
    walks = []
    for i in range(num_walks):
        nodes = np.random.permutation(graph_nodes)
        for j in range(n_nodes):
            walk = random_walk(graph, nodes[j], walk_length)
            walks.append(walk)
    return walks


# = = = = = = = = = = = = = = =

# pad_vec_idx = 1685894  # 0-based index of the last row of the embedding matrix (for zero-padding)
#
# # parameters
# nb_walks = 5
# walks_length = 10
# max_doc_size = 70  # maximum number of 'sentences' (walks) in each pseudo-document
#
# path_root = ''  # fill me!
# path_to_data = path_root + 'data/'


# = = = = = = = = = = = = = = =

def generate_doc_n2v(edge_list, path_to_data, nb_walks=5, walks_length=10, p=2, q=0.5):
    """ Create a document by generating walks

    :param q:
    :param p:
    :param edge_list:
    :param path_to_data:
    :param nb_walks:
    :param walks_length:
    :return doc:
    """
    # construct graph from edgelist
    g_n2v = n2v.Graph(nx.read_edgelist(path_to_data + 'edge_lists/' + edge_list), is_directed=False, p=p, q=q)
    # create the pseudo-document representation of the graph
    doc = g_n2v.simulate_walks(nb_walks, walks_length, verbose=0)

    return doc


def generate_doc(edge_list, path_to_data, nb_walks=5, walks_length=10):
    """ Create a document by generating walks

    :param edge_list:
    :param path_to_data:
    :param nb_walks:
    :param walks_length:
    :return doc:
    """
    # construct graph from edgelist
    g = nx.read_edgelist(path_to_data + 'edge_lists/' + edge_list)
    # create the pseudo-document representation of the graph
    doc = generate_walks(g, nb_walks, walks_length)

    return doc


def generate_multi_docs(edge_lists, path_to_data, nb_walks=5, walks_length=10, verbose=1):
    if verbose:
        enum = enumerate(tqdm(edge_lists, desc="Generating Walks"))
    else:
        enum = enumerate(edge_lists)

    docs = []
    for idx, edge_list in enum:
        docs.append(generate_doc(edge_list, path_to_data, nb_walks, walks_length))

        if idx % round(len(edge_lists) / 10) == 0:
            print('Reading edge list : %.0f %%' % (100 * idx / len(edge_lists)))

    return docs


def progress_callback(tqdm_p_bar):
    tqdm_p_bar.update(1)


def preprocessing(train_config: EasyDict, multi_processing=True):
    start_time = time()

    edgelists = os.listdir(train_config.path_data + 'edge_lists/')
    edgelists.sort(key=natural_keys)  # important to maintain alignment with the targets!

    if multi_processing:
        # We generate the documents by multiprocessing
        print('Beginning Multi-preocessing')
        pool = Pool(processes=4)

        if train_config.node2vec:
            partial_gen_docs_funct = partial(generate_doc_n2v, path_to_data=train_config.path_data,
                                             nb_walks=train_config.nb_walks, walks_length=train_config.walks_length,
                                             p=train_config.p_n2v, q=train_config.q_n2v)
        else:
            partial_gen_docs_funct = partial(generate_doc, path_to_data=train_config.path_to_data,
                                             nb_walks=train_config.nb_walks, walks_length=train_config.walks_length)

        # noinspection PyTypeChecker
        docs = pool.map(partial_gen_docs_funct, edgelists)

    else:
        # Mono-processing
        docs = generate_multi_docs(edgelists, train_config.path_data, train_config.nb_walks,
                                   train_config.walks_length, verbose=1)

    print('Documents generated')

    # truncation-padding at the document level, i.e., adding or removing entire 'sentences'
    docs = [
        d + [[train_config.pad_vec_idx] * (train_config.walks_length + 1)] * (train_config.max_doc_size - len(d))
        if len(d) < train_config.max_doc_size else d[:train_config.max_doc_size]
        for d in docs]

    docs = np.array(docs).astype('int')
    print('Document array shape:', docs.shape)

    filename = train_config.path_data + 'documents_' + str(train_config.nb_walks) + '_' + str(train_config.walks_length)
    np.save(filename, docs, allow_pickle=False)

    print('Documents saved in ' + filename + '.npy')
    print('Everything done in', round(time() - start_time, 2))

    return docs


# = = = = = = = = = = = = = = =

if __name__ == '__main__':
    preprocessing(src.main.train_config)
