import json

import numpy as np
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.layers import Input, Embedding, Dropout, Bidirectional, GRU, CuDNNGRU, TimeDistributed, Dense
from keras.models import Model

from src.AttentionWithContext import AttentionWithContext


def bidir_gru(my_seq, n_units=50, is_gpu=True):
    """ Returns a convenient wrapper for bidirectional RNN with GRU units
    enables CUDA acceleration on GPU
    # regardless of whether training is done on GPU, model can be loaded on CPU
    # see: https://github.com/keras-team/keras/pull/9112
    """
    if is_gpu:
        return Bidirectional(CuDNNGRU(units=n_units,
                                      return_sequences=True),
                             merge_mode='concat', weights=None)(my_seq)
    else:
        return Bidirectional(GRU(units=n_units,
                                 activation='tanh',
                                 dropout=0.0,
                                 recurrent_dropout=0.0,
                                 implementation=1,
                                 return_sequences=True,
                                 reset_after=True,
                                 recurrent_activation='sigmoid'),
                             merge_mode='concat', weights=None)(my_seq)


def save_hist(history, filename):
    try:
        history = history.history
    except AttributeError:
        pass

    with open(filename + '.json', 'w') as file_write:
        json.dump(history, file_write, sort_keys=False, indent=4)


def load_hist(filename):
    with open(filename + '.json', 'r') as file_read:
        history = json.load(file_read)

    return history


def data_loading(path_to_data='data/', save_name='documents'):
    """ Data loading, returns the documents, the embeddings and the train indexes

    :param save_name:
    :param path_to_data:
    :return docs:
    :return embeddings:
    :return train_idxs:
    """
    docs = np.load(path_to_data + save_name + '.npy')
    embeddings = np.load(path_to_data + 'embeddings.npy')

    with open(path_to_data + 'train_idxs.txt', 'r') as file:
        train_idxs = file.read().splitlines()

    train_idxs = [int(elt) for elt in train_idxs]

    return docs, embeddings, train_idxs


# create validation set
def create_val_set(train_idxs, docs, ran_seed=12219):
    """ Separate the training set and validation set

    :param train_idxs:
    :param docs:
    :param ran_seed:
    :return idxs_select_train:
    :return idxs_select_val:
    :return docs_train:
    :return docs_val:
    """
    # We use a seed in order to get always the same repartition train/val
    np.random.seed(ran_seed)
    idxs_select_train = np.random.choice(range(len(train_idxs)), size=int(len(train_idxs) * 0.80), replace=False)
    idxs_select_val = np.setdiff1d(range(len(train_idxs)), idxs_select_train)

    train_idxs_new = [train_idxs[elt] for elt in idxs_select_train]
    val_idxs = [train_idxs[elt] for elt in idxs_select_val]

    docs_train = docs[train_idxs_new, :, :]
    docs_val = docs[val_idxs, :, :]

    return idxs_select_train, idxs_select_val, docs_train, docs_val


def create_model(docs_train, embeddings, train_config):
    """ Create the model. This is actually the original given Hierarchical Attention Network (HAN).
    In the future, may be modified or duplicated to incoporate different architectures

    :param train_config:
    :param docs_train:
    :param embeddings:
    :return:
    """

    # Input and embedding, using the previously computed embedding
    sent_ints = Input(shape=(docs_train.shape[2],))

    sent_wv = Embedding(input_dim=embeddings.shape[0],
                        output_dim=embeddings.shape[1],
                        weights=[embeddings],
                        input_length=docs_train.shape[2],
                        trainable=False,
                        )(sent_ints)

    sent_wv_dr = Dropout(train_config.drop_rate)(sent_wv)

    # Bidirectional RNN, followed by Attention
    sent_wa = bidir_gru(sent_wv_dr, train_config.n_units, train_config.is_GPU)
    sent_att_vec, word_att_coeffs = AttentionWithContext(return_coefficients=True)(sent_wa)
    sent_att_vec_dr = Dropout(train_config.drop_rate)(sent_att_vec)

    # The sentences Encoder
    sent_encoder = Model(sent_ints, sent_att_vec_dr)

    # Same architecture, for documents : Input, TimeDistributed (with sentences as inputs)
    doc_ints = Input(shape=(docs_train.shape[1], docs_train.shape[2],))
    sent_att_vecs_dr = TimeDistributed(sent_encoder)(doc_ints)

    # Then RNN and Attention
    doc_sa = bidir_gru(sent_att_vecs_dr, train_config.n_units, train_config.is_GPU)
    doc_att_vec, sent_att_coeffs = AttentionWithContext(return_coefficients=True)(doc_sa)
    doc_att_vec_dr = Dropout(train_config.drop_rate)(doc_att_vec)

    # The final Fully Connected Layer
    preds = Dense(units=1,
                  activation='sigmoid')(doc_att_vec_dr)

    # The final Model, taking as Input the documents, and giving a prediction
    model = Model(doc_ints, preds)
    model.compile(loss='mean_squared_error',
                  optimizer=train_config.my_optimizer,
                  metrics=['mae'])

    print('model compiled')

    return model


def model_training(model, docs_train, target_train, docs_val, target_val, target_nb, train_config, verbose=2):
    """

    :param train_config:
    :param model:
    :param docs_train:
    :param target_train:
    :param docs_val:
    :param target_val:
    :param target_nb:
    :param verbose:
    :return:
    """

    # An early stopping condition
    early_stopping = EarlyStopping(monitor='val_loss',
                                   patience=train_config.my_patience,
                                   mode='min')

    print(target_nb)

    # save model corresponding to best epoch
    checkpointer = ModelCheckpoint(filepath=train_config.path_data + 'model_' + str(target_nb),
                                   verbose=1,
                                   save_best_only=True,
                                   save_weights_only=True)

    if train_config.save_weights:
        my_callbacks = [early_stopping, checkpointer]
    else:
        my_callbacks = [early_stopping]

    # We fit the model to the data
    hist = model.fit(docs_train,
                     target_train,
                     batch_size=train_config.batch_size,
                     epochs=train_config.nb_epochs,
                     validation_data=(docs_val, target_val),
                     callbacks=my_callbacks,
                     verbose=verbose)

    # We retrieve the history
    hist_to_save = hist.history
    save_file_name = train_config.save_file_name + '_' + str(target_nb)

    if train_config.save_history:
        save_hist(hist_to_save, train_config.path_data + save_file_name)

    return hist


def train_4_targets(idxs_select_train, idxs_select_val, docs_train, docs_val, embeddings, train_config):
    """ This function trains the 4 targets with similar models. It should probably not be used.

    :param train_config:
    :param idxs_select_train:
    :param idxs_select_val:
    :param docs_train:
    :param embeddings:
    :param docs_val:
    :return:
    """
    for tgt in range(4):
        with open(train_config.path_data + 'targets/train/target_' + str(tgt) + '.txt', 'r') as file:
            target = file.read().splitlines()

        target_train = np.array([target[elt] for elt in idxs_select_train]).astype('float')
        target_val = np.array([target[elt] for elt in idxs_select_val]).astype('float')

        print('data loaded')

        # = = = = = defining architecture = = = = =

        model = create_model(docs_train, embeddings, train_config)

        model_training(model, docs_train, target_train, docs_val, target_val, tgt, train_config, verbose=2)

        print('* * * * * * * target', tgt, 'done * * * * * * *')


# if __name__ == '__main__':
#
#     # ===== System Parameters =====
#
#     is_GPU = True
#     save_weights = True
#     save_history = True
#
#     path_root = ''  # fill me!
#     path_to_code = path_root + 'src/'
#     path_to_data = path_root + 'data/'
#
#     # sys.path.insert(0, path_to_code)
#
#     # ===== Network parameters =====
#
#     n_units = 50
#     drop_rate = 0.5
#     batch_size = 96
#     nb_epochs = 10
#     my_patience = 4
#
#     # Testing SGD
#     # my_optimizer = SGD(lr=0.001, momentum=0.0, decay=0.0, nesterov=False)
#     my_optimizer = Adam(lr=0.001)
#
#     # ===== Datasets =====
#
#     docs, embeddings, train_idxs = data_loading(path_to_data)
#     idxs_select_train, idxs_select_val, docs_train, docs_val = create_val_set(train_idxs, docs)
#
#     # ===== Models and training =====
#
#     for tgt in range(4):
#         with open(path_to_data + 'targets/train/target_' + str(tgt) + '.txt', 'r') as file:
#             target = file.read().splitlines()
#
#         target_train = np.array([target[elt] for elt in idxs_select_train]).astype('float')
#         target_val = np.array([target[elt] for elt in idxs_select_val]).astype('float')
#
#         print('data loaded')
#
#         # = = = = = defining architecture = = = = =
#
#         model = create_model(docs_train, embeddings, drop_rate, n_units, is_GPU)
#
#         model_training(model, docs_train, target_train, docs_val, target_val, tgt, path_to_data, my_patience,
#                        save_weights, batch_size,
#                        nb_epochs, verbose=2)
#
#         print('* * * * * * * target', tgt, 'done * * * * * * *')
