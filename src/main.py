from src.test_functions import *
from src.util import EasyDict

train_config = EasyDict(
    do_preprocessing=False,
    nb_walks=5,
    walks_length=10,
    max_doc_size=70,
    pad_vec_idx=1685894,

    node2vec=True,
    p_n2v=2,
    q_n2v=0.5,

    n_units=50,
    drop_rate=0.5,
    batch_size=96,
    nb_epochs=10,
    my_patience=4,
    my_optimizer='adam',

    is_GPU=True,
    save_weights=True,
    save_history=True,
    path_data='data/',

    document_file_name='documents',
    save_file_name='model_hist'
)

if __name__ == '__main__':

    file_name = 'documents_' + str(train_config.nb_walks) + '_' + str(train_config.walk_length)

    train_config.do_preprocessing = True
    train_config.n_units = 20

    history = training(train_config)

    plot_from_list(history, do_show=True, title=file_name)

    multiTest = False
    if multiTest:

        list_nb_walks = [3, 5, 10]
        list_walk_lengths = [5, 10, 15]

        for nb_walks in list_nb_walks:
            for walk_length in list_walk_lengths:
                print("##################### New set of NW / WL ########################")
                # We create the name of the save files

                # We choose the right documents and train
                file_name = 'documents_' + str(nb_walks) + '_' + str(walk_length)
                do_preproc = (nb_walks == 10) and (walk_length == 15)
                history = training(train_config)

                plot_from_list(history, do_show=True, title=file_name)

        plt.show()
