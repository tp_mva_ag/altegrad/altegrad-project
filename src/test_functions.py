from src.main_baseline import *
from src.preprocessing_baseline import preprocessing
import matplotlib.pyplot as plt

from src.util import EasyDict


def plot_history(history, do_show=True, title=''):
    try:
        history = history.history
    except AttributeError:
        pass

    plt.figure()
    plt.plot(history['mean_absolute_error'])
    plt.plot(history['val_mean_absolute_error'])
    plt.title(title + ' model mean absolute error')
    plt.ylabel('mean absolute error')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')

    # summarize history for loss
    plt.figure()
    plt.plot(history['loss'])
    plt.plot(history['val_loss'])
    plt.title(title + ' model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')

    if do_show:
        plt.show(block=False)


def plot_from_list(histories, do_show=True, title=''):
    try:
        histories = [history.history for history in histories]
    except AttributeError:
        pass

    color_train = 'xkcd:blue'
    color_val = 'xkcd:orange'
    line_train = ['o-', '^-', 'o--', '^--']
    line_val = ['o-', '^-', 'o--', '^--']
    legend = []
    for i in range(4):
        legend.append('train tgt #' + str(i))
    for i in range(4):
        legend.append('val tgt #' + str(i))

    plt.figure()
    for i in range(4):
        plt.plot(histories[i]['mean_absolute_error'], line_train[i], color=color_train)
    for i in range(4):
        plt.plot(histories[i]['val_mean_absolute_error'], line_val[i], color=color_val)

    plt.title(title + ' model mean absolute error')
    plt.ylabel('mean absolute error')
    plt.xlabel('epoch')
    plt.legend(legend, loc='upper left')

    # summarize history for loss
    plt.figure()
    for i in range(4):
        plt.plot(histories[i]['loss'], line_train[i], color=color_train)
    for i in range(4):
        plt.plot(histories[i]['val_loss'], line_val[i], color=color_val)

    plt.title(title + ' model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(legend, loc='upper left')

    if do_show:
        plt.show(block=False)


def plot_from_files(file_name_list):
    """ Load 4 JSON files and plot them

    :param file_name_list:
    :return:
    """
    histories = [load_hist(file_name) for file_name in file_name_list]
    plot_from_list(histories)


def load_target(path_data, target_nb, idxs_train, idxs_val):
    """

    :param path_data:
    :param target_nb:
    :param idxs_train:
    :param idxs_val:
    :return:
    """
    with open(path_data + 'targets/train/target_' + str(target_nb) + '.txt', 'r') as file_target:
        target_list = file_target.read().splitlines()

    target_train = np.array([target_list[elt] for elt in idxs_train]).astype('float')
    target_val = np.array([target_list[elt] for elt in idxs_val]).astype('float')

    print('data loaded')

    return target_train, target_val


def training(train_config: EasyDict):
    """

    :param train_config:
    :return:
    """

    # ===== Preprocessing =====

    if train_config.do_preprocessing:
        preprocessing(train_config)

    # ===== Datasets =====

    docs, embeddings, train_idxs = data_loading(train_config.path_data, save_name=train_config.document_file_name)
    idxs_select_train, idxs_select_val, docs_train, docs_val = create_val_set(train_idxs, docs)

    # ===== Models and training =====

    history_list = []

    for tgt in range(4):
        # Loading targets
        target_train, target_val = load_target(train_config.path_data, tgt, idxs_select_train, idxs_select_val)

        # Creating model
        model = create_model(docs_train, embeddings, train_config)

        # Training
        history = model_training(model, docs_train, target_train, docs_val, target_val, tgt, train_config)

        history_list.append(history)

        # plot_history(history)
        print('* * * * * * * target', tgt, 'done * * * * * * *')

    return history_list


def get_min_mse(history):
    val_mse = history['val_loss']

    min_val_mse = min(val_mse)

    best_epoch = val_mse.index(min_val_mse) + 1

    return min_val_mse, best_epoch


def save_multi_preprocessing(list_nb_walks, list_walk_lengths, train_config):
    """

    :param train_config:
    :param list_nb_walks:
    :param list_walk_lengths:
    :type list_nb_walks: list
    :type list_walk_lengths: list
    """

    len_nb_walks = len(list_nb_walks)
    len_wk_length = len(list_walk_lengths)

    # We go through all the possible combinations to create the corresponding documents
    for idx_nw, nb_walks in enumerate(list_nb_walks):
        for idx_wl, walk_length in enumerate(list_walk_lengths):
            print('***** %.0f %% done *****' % ((idx_nw * len_wk_length + idx_wl) / (len_nb_walks * len_wk_length)))

            train_config.nb_walks = nb_walks
            train_config.walks_length = walk_length

            preprocessing(train_config)
